package main

import (
	"fmt"
	"testing"
)

func TestTestDataBaseConn(t *testing.T) {
	if err := TestDataBaseConn(DataBaseConfig{Host: "127.0.0.1", Username: "root", Password: "ro1ot", DbName: "micro_devops"}); err != nil {
		fmt.Println("连接失败")
		fmt.Println(err.Error())
	} else {
		fmt.Println("连接成功")
	}
}
