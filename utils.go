package main

import (
	"archive/zip"
	log "github.com/sirupsen/logrus"
	"github.com/ying32/govcl/vcl"
	"io"
	"math"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

func DeleteSpecialChar(str string) string {
	str = strings.Replace(str, " ", "", -1)
	str = strings.Replace(str, "\t", "", -1)
	str = strings.Replace(str, "\n", "", -1)
	return strings.Replace(str, "\r", "", -1)
}

func GetArch() int {
	return 32 << (^uint(0) >> 63)
}

func Unzip(src, dest string) error {
	srcReader, err := zip.OpenReader(src)
	if err != nil {
		return err
	}
	defer srcReader.Close()
	for _, srcFile := range srcReader.File {
		srcFileReader, err := srcFile.Open()
		if err != nil {
			return err
		}
		destPath := filepath.Join(dest, srcFile.Name)
		if srcFile.FileInfo().IsDir() {
			_ = os.MkdirAll(destPath, srcFile.Mode())
		} else {
			var destFileDir string
			if lastIndex := strings.LastIndex(destPath, string(os.PathSeparator)); lastIndex > -1 {
				destFileDir = destPath[:lastIndex]
			}
			err = os.MkdirAll(destFileDir, srcFile.Mode())
			if err != nil {
				log.Fatal(err)
				return err
			}
			destFile, err := os.OpenFile(destPath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, srcFile.Mode())
			if err != nil {
				return err
			}
			_, err = io.Copy(destFile, srcFileReader)
			if err != nil {
				return err
			}
			destFile.Close()
		}
		srcFileReader.Close()
	}
	return nil
}

func DownloadFile(downloadUrl, filePath, fileName string, process *vcl.TProgressBar) {
	var (
		err  error
		resp *http.Response
		size int64
		file *os.File
	)
	if _, err = os.Stat(filePath); err != nil {
		_ = os.MkdirAll(filePath, 0775)
	}
	client := http.DefaultClient
	client.Timeout = time.Duration(180) * time.Second
	if resp, err = client.Get(downloadUrl); err != nil {
		log.Errorln(err.Error())
		return
	}
	contentLength := resp.Header.Get("Content-Length")
	if size, err = strconv.ParseInt(contentLength, 10, 64); err != nil {
		log.Errorln(contentLength, err.Error())
		return
	}
	finalPath := filepath.Join(filePath, fileName)
	if file, err = os.Create(finalPath); err != nil {
		log.Errorln(finalPath, err.Error())
		return
	}
	defer file.Close()
	if resp.Body == nil {
		log.Errorln("请求连接返回结构体为空")
		return
	}
	defer resp.Body.Close()
	var written int64
	var buf = make([]byte, 32*1024)
	for {
		nr, er := resp.Body.Read(buf)
		if nr > 0 {
			nw, ew := file.Write(buf[0:nr])
			if nw > 0 {
				written += int64(nw)
			}
			if ew != nil {
				err = ew
				break
			}
			if nr != nw {
				err = io.ErrShortWrite
				break
			}
		}
		if er != nil {
			if er != io.EOF {
				err = er
			}
			break
		}
		vcl.ThreadSync(func() {
			process.SetPosition(int32(math.Floor(float64(written) / float64(size) * 100)))
		})
	}
	return
}
