# toolkit

#### 介绍
日常开发工具

很多功能暂未实现，用于研究Golang-GUI的实现方案

#### 软件架构
Golang

#### 依赖库
1. UI库: https://github.com/ying32/govcl/vcl
2. IP地址查询库: https://gitee.com/lionsoul/ip2region
3. 应用数据库: https://github.com/syndtr/goleveldb/leveldb
4. Html解析库: https://github.com/PuerkitoBio/goquery


#### 安装教程
1.  git clone https://gitee.com/1967988842/toolkit.git
2.  go mod tidy
3.  go mod vendor
4.  执行build目录下build.bat打包并运行

#### 使用说明
1.  UI相关都在toolkit_ui.go文件中
2.  encode目录是编码解码实现类
3.  magnet目录是磁力链接搜索实现类

#### 参与贡献
1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 截图示例
##### 代码生成器
![输入图片说明](https://images.gitee.com/uploads/images/2020/0517/132049_17b7ffb9_673473.png "1.png")
##### SDK管理
![输入图片说明](https://images.gitee.com/uploads/images/2020/0517/132117_9dd8e2b5_673473.png "2.png")
##### 磁力链接搜索
![输入图片说明](https://images.gitee.com/uploads/images/2020/0517/132135_7942b863_673473.png "3.png")
##### 开发小工具
![输入图片说明](https://images.gitee.com/uploads/images/2020/0517/132158_cac1bd9f_673473.png "4.png")
##### 加密解密
![输入图片说明](https://images.gitee.com/uploads/images/2020/0517/132217_80cac780_673473.png "5.png")





