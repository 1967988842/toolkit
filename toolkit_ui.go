package main

import (
	"encoding/json"
	"fmt"
	_ "github.com/ying32/govcl/pkgs/winappres"
	"github.com/ying32/govcl/vcl"
	"github.com/ying32/govcl/vcl/types"
	"path/filepath"
	"toolkit/encode"
	"toolkit/magnet"
)

var mainForm *MainForm

const (
	Width           = 800
	Height          = 450
	EmptyWidth      = 0
	LabelFix        = 5
	WidthSpace      = 30
	FixWidthSpace   = 25
	CommonHeight    = 50
	LabelWidth      = 50
	FormBtnWidth    = 80
	FormBtnHeight   = 23
	EditWidth       = 250
	WindowBtnWidth  = 80
	WindowBtnHeight = 23
	ComboBoxWidth   = 150
)

type MainForm struct {
	*vcl.TForm
	S1_DataUrlEdit         *vcl.TEdit
	S1_DataNameEdit        *vcl.TEdit
	S1_DataUsernameEdit    *vcl.TEdit
	S1_DataPasswordEdit    *vcl.TEdit
	S1_BasePathEdit        *vcl.TEdit
	S1_ChooseBasePathBtn   *vcl.TButton
	S1_BasePackageNameEdit *vcl.TEdit
	S1_PackageNameEdit     *vcl.TEdit
	S1_TablesEdit          *vcl.TEdit
	S1_ChooseTablesBtn     *vcl.TButton
	S1_TestDatabaseConnBtn *vcl.TButton
	S1_GenJavaCodeBtn      *vcl.TButton
	S1_GenGoCodeBtn        *vcl.TButton
	S1_OpenBasePathBtn     *vcl.TButton
	S2_RootPathEdit        *vcl.TEdit
	S2_ChooseRootPathBtn   *vcl.TButton
	S2_RepoPathEdit        *vcl.TEdit
	S2_ChooseRepoPathBtn   *vcl.TButton
	S2_GetNodeJSLSVBtn     *vcl.TButton
	S2_GetNodeJSVersionBtn *vcl.TButton
	S2_DownloadNodeJSBtn   *vcl.TButton
	S3_MagnetHostComboBox  *vcl.TComboBox
	S3_MagnetSearchKeyEdit *vcl.TEdit
	S3_MagnetSearchBtn     *vcl.TButton
	S3_MagnetSearchNextBtn *vcl.TButton
	S3_MagnetListTable     *vcl.TStringGrid
	S4_Ip2RegionEdit       *vcl.TEdit
	S4_Ip2RegionBtn        *vcl.TButton
	S5_EncodeSourceMemo    *vcl.TMemo
	S5_EncodeTypeComboBox  *vcl.TComboBox
	S5_EncodeBtn           *vcl.TButton
	S5_DecodeBtn           *vcl.TButton
	S5_EncodeResultMemo    *vcl.TMemo
}

func UIInit() {
	vcl.RunApp(&mainForm)
}

func (mainForm *MainForm) OnFormCreate(_ vcl.IObject) {
	// 设置窗口基础信息
	mainForm.SetCaption("工具箱")
	mainForm.SetPosition(types.PoScreenCenter)
	mainForm.SetWidth(Width)
	mainForm.SetHeight(Height)

	// 设置窗口面板
	pgc := vcl.NewPageControl(mainForm)
	pgc.SetParent(mainForm)
	pgc.SetAlign(types.AlClient)

	// 设置首页
	sheet := vcl.NewTabSheet(mainForm)
	sheet.SetPageControl(pgc)
	sheet.SetCaption("首页")

	var top int32 = 20

	// 设置面板[代码生成器]
	if S1 {
		sheet = vcl.NewTabSheet(mainForm)
		sheet.SetPageControl(pgc)
		sheet.SetCaption("代码生成器")
		sheet.SetOnShow(func(sender vcl.IObject) {
			go mainForm.renderS1Sheet()
		})

		S1_DataUrlLabel := vcl.NewLabel(mainForm)
		S1_DataUrlLabel.SetParent(sheet)
		S1_DataUrlLabel.SetBounds(WidthSpace, top+LabelFix, LabelWidth, CommonHeight)
		S1_DataUrlLabel.SetCaption("数据库地址")

		mainForm.S1_DataUrlEdit = vcl.NewEdit(mainForm)
		mainForm.S1_DataUrlEdit.SetParent(sheet)
		mainForm.S1_DataUrlEdit.SetBounds(S1_DataUrlLabel.Left()+S1_DataUrlLabel.Width()+WidthSpace, top, EditWidth, EmptyWidth)

		S1_DataNameLabel := vcl.NewLabel(mainForm)
		S1_DataNameLabel.SetParent(sheet)
		S1_DataNameLabel.SetBounds(mainForm.S1_DataUrlEdit.Left()+mainForm.S1_DataUrlEdit.Width()+WidthSpace, top+LabelFix, LabelWidth, EmptyWidth)
		S1_DataNameLabel.SetCaption("数据库名称")

		mainForm.S1_DataNameEdit = vcl.NewEdit(mainForm)
		mainForm.S1_DataNameEdit.SetParent(sheet)
		mainForm.S1_DataNameEdit.SetBounds(S1_DataNameLabel.Left()+S1_DataNameLabel.Width()+WidthSpace, top, EditWidth, EmptyWidth)

		top += CommonHeight

		S1_DataUsernameLabel := vcl.NewLabel(mainForm)
		S1_DataUsernameLabel.SetParent(sheet)
		S1_DataUsernameLabel.SetBounds(WidthSpace, top+LabelFix, LabelWidth, EmptyWidth)
		S1_DataUsernameLabel.SetCaption("数据库账号")

		mainForm.S1_DataUsernameEdit = vcl.NewEdit(mainForm)
		mainForm.S1_DataUsernameEdit.SetParent(sheet)
		mainForm.S1_DataUsernameEdit.SetBounds(S1_DataUsernameLabel.Left()+S1_DataUsernameLabel.Width()+WidthSpace, top, EditWidth, EmptyWidth)

		S1_DataPasswordLabel := vcl.NewLabel(mainForm)
		S1_DataPasswordLabel.SetParent(sheet)
		S1_DataPasswordLabel.SetBounds(mainForm.S1_DataUsernameEdit.Left()+mainForm.S1_DataUsernameEdit.Width()+WidthSpace, top+LabelFix, LabelWidth, EmptyWidth)
		S1_DataPasswordLabel.SetCaption("数据库密码")

		mainForm.S1_DataPasswordEdit = vcl.NewEdit(mainForm)
		mainForm.S1_DataPasswordEdit.SetParent(sheet)
		mainForm.S1_DataPasswordEdit.SetPasswordChar('*')
		mainForm.S1_DataPasswordEdit.SetBounds(S1_DataPasswordLabel.Left()+S1_DataPasswordLabel.Width()+WidthSpace, top, EditWidth, EmptyWidth)

		top += CommonHeight
		S1_BasePathLabel := vcl.NewLabel(mainForm)
		S1_BasePathLabel.SetParent(sheet)
		S1_BasePathLabel.SetBounds(WidthSpace, top+LabelFix, LabelWidth, EmptyWidth)
		S1_BasePathLabel.SetCaption("生成位置")

		mainForm.S1_BasePathEdit = vcl.NewEdit(mainForm)
		mainForm.S1_BasePathEdit.SetParent(sheet)
		mainForm.S1_BasePathEdit.SetReadOnly(true)
		mainForm.S1_BasePathEdit.SetBounds(S1_BasePathLabel.Left()+S1_BasePathLabel.Width()+WidthSpace, top, EditWidth, EmptyWidth)

		mainForm.S1_ChooseBasePathBtn = vcl.NewButton(mainForm)
		mainForm.S1_ChooseBasePathBtn.SetParent(sheet)
		mainForm.S1_ChooseBasePathBtn.SetBounds(mainForm.S1_BasePathEdit.Left()+mainForm.S1_BasePathEdit.Width()+FixWidthSpace, top, FormBtnWidth, FormBtnHeight)
		mainForm.S1_ChooseBasePathBtn.SetCaption("选择")
		mainForm.S1_ChooseBasePathBtn.SetOnClick(func(_ vcl.IObject) {
			chooseFolder(mainForm.S1_BasePathEdit)
		})

		top += CommonHeight
		S1_BasePackageNameLabel := vcl.NewLabel(mainForm)
		S1_BasePackageNameLabel.SetParent(sheet)
		S1_BasePackageNameLabel.SetBounds(WidthSpace, top+LabelFix, LabelWidth, EmptyWidth)
		S1_BasePackageNameLabel.SetCaption("生成包名")

		mainForm.S1_BasePackageNameEdit = vcl.NewEdit(mainForm)
		mainForm.S1_BasePackageNameEdit.SetParent(sheet)
		mainForm.S1_BasePackageNameEdit.SetBounds(S1_BasePackageNameLabel.Left()+S1_BasePackageNameLabel.Width()+WidthSpace, top, EditWidth, EmptyWidth)

		S1_PackageNameLabel := vcl.NewLabel(mainForm)
		S1_PackageNameLabel.SetParent(sheet)
		S1_PackageNameLabel.SetBounds(mainForm.S1_BasePackageNameEdit.Left()+mainForm.S1_BasePackageNameEdit.Width()+WidthSpace, top+LabelFix, LabelWidth, EmptyWidth)
		S1_PackageNameLabel.SetCaption("生成包名称")

		mainForm.S1_PackageNameEdit = vcl.NewEdit(mainForm)
		mainForm.S1_PackageNameEdit.SetParent(sheet)
		mainForm.S1_PackageNameEdit.SetBounds(S1_PackageNameLabel.Left()+S1_PackageNameLabel.Width()+WidthSpace, top, EditWidth, EmptyWidth)

		top += CommonHeight
		S1_TablesLabel := vcl.NewLabel(mainForm)
		S1_TablesLabel.SetParent(sheet)
		S1_TablesLabel.SetBounds(WidthSpace, top+LabelFix, LabelWidth, EmptyWidth)
		S1_TablesLabel.SetCaption("数据库表")

		mainForm.S1_TablesEdit = vcl.NewEdit(mainForm)
		mainForm.S1_TablesEdit.SetParent(sheet)
		mainForm.S1_TablesEdit.SetBounds(S1_TablesLabel.Left()+S1_TablesLabel.Width()+WidthSpace, top, EditWidth, EmptyWidth)

		mainForm.S1_ChooseTablesBtn = vcl.NewButton(mainForm)
		mainForm.S1_ChooseTablesBtn.SetParent(sheet)
		mainForm.S1_ChooseTablesBtn.SetBounds(mainForm.S1_TablesEdit.Left()+mainForm.S1_TablesEdit.Width()+FixWidthSpace, top, FormBtnWidth, FormBtnHeight)
		mainForm.S1_ChooseTablesBtn.SetCaption("选择")
		mainForm.S1_ChooseTablesBtn.SetOnClick(func(_ vcl.IObject) {
			// 检查数据库连接信息是否填写正确
			if err := TestDataBaseConn(
				DataBaseConfig{
					Host:     mainForm.S1_DataUrlEdit.Text(),
					Username: mainForm.S1_DataUsernameEdit.Text(),
					Password: mainForm.S1_DataPasswordEdit.Text(),
					DbName:   mainForm.S1_DataNameEdit.Text(),
				}); err != nil {
				vcl.ShowMessage("数据库连接失败" + LineBreak + err.Error())
				return
			}

			// 创建新的窗口展示数据
			tablesForm := vcl.NewForm(mainForm)
			tablesForm.SetCaption("选择数据库表")
			tablesForm.SetPosition(types.PoScreenCenter)
			tablesForm.SetWidth(250)
			tablesForm.SetHeight(400)

			// 数据库表列表
			tablesChkListBox := vcl.NewCheckListBox(tablesForm)
			tablesChkListBox.SetParent(tablesForm)
			tablesChkListBox.SetAlign(types.AlClient)

			go mainForm.asyncRenderTables(tablesChkListBox)

			selectAllBtn := vcl.NewButton(tablesForm)
			selectAllBtn.SetParent(tablesForm)
			selectAllBtn.SetCaption("全选")
			selectAllBtn.SetAlign(types.AlBottom)
			selectAllBtn.SetOnClick(func(sender vcl.IObject) {
				tablesChkListBox.CheckAll(types.CbChecked, true, true)
			})

			deselectAllBtn := vcl.NewButton(tablesForm)
			deselectAllBtn.SetParent(tablesForm)
			deselectAllBtn.SetCaption("取消全选")
			deselectAllBtn.SetAlign(types.AlBottom)
			deselectAllBtn.SetOnClick(func(sender vcl.IObject) {
				tablesChkListBox.CheckAll(types.CbUnchecked, true, true)
			})

			confirmBtn := vcl.NewButton(tablesForm)
			confirmBtn.SetParent(tablesForm)
			confirmBtn.SetCaption("确定")
			confirmBtn.SetAlign(types.AlBottom)
			confirmBtn.SetOnClick(func(sender vcl.IObject) {
				tablesForm.Close()
			})

			tablesForm.Show()
			tablesForm.SetOnClose(func(sender vcl.IObject, action *types.TCloseAction) {
				fmt.Println("关闭窗口,需要获取选中的表")
			})
		})

		top += CommonHeight * 1.5
		mainForm.S1_TestDatabaseConnBtn = vcl.NewButton(mainForm)
		mainForm.S1_TestDatabaseConnBtn.SetParent(sheet)
		mainForm.S1_TestDatabaseConnBtn.SetBounds(WidthSpace*5, top, WindowBtnWidth, WindowBtnHeight)
		mainForm.S1_TestDatabaseConnBtn.SetCaption("测试连接")
		mainForm.S1_TestDatabaseConnBtn.SetOnClick(func(_ vcl.IObject) {
			// 检查参数是否都填写完成
			mainForm.S1_TestDatabaseConnBtn.SetCaption("正在测试")
			mainForm.S1_TestDatabaseConnBtn.SetEnabled(false)
			if err := TestDataBaseConn(
				DataBaseConfig{
					Host:     mainForm.S1_DataUrlEdit.Text(),
					Username: mainForm.S1_DataUsernameEdit.Text(),
					Password: mainForm.S1_DataPasswordEdit.Text(),
					DbName:   mainForm.S1_DataNameEdit.Text(),
				}); err != nil {
				vcl.ShowMessage("数据库连接失败" + LineBreak + err.Error())
				mainForm.S1_TestDatabaseConnBtn.SetCaption("测试连接")
				mainForm.S1_TestDatabaseConnBtn.SetEnabled(true)
			} else {
				vcl.ShowMessage("数据库连接成功")
				mainForm.S1_TestDatabaseConnBtn.SetCaption("测试连接")
				mainForm.S1_TestDatabaseConnBtn.SetEnabled(true)
			}
		})

		mainForm.S1_GenJavaCodeBtn = vcl.NewButton(mainForm)
		mainForm.S1_GenJavaCodeBtn.SetParent(sheet)
		mainForm.S1_GenJavaCodeBtn.SetBounds(mainForm.S1_TestDatabaseConnBtn.Left()+WidthSpace*2+mainForm.S1_TestDatabaseConnBtn.Width(), top, WindowBtnWidth, WindowBtnHeight)
		mainForm.S1_GenJavaCodeBtn.SetCaption("生成Java代码")
		mainForm.S1_GenJavaCodeBtn.SetOnClick(func(_ vcl.IObject) {
			fmt.Println("暂未实现,生成Java代码")
		})

		mainForm.S1_GenGoCodeBtn = vcl.NewButton(mainForm)
		mainForm.S1_GenGoCodeBtn.SetParent(sheet)
		mainForm.S1_GenGoCodeBtn.SetBounds(mainForm.S1_GenJavaCodeBtn.Left()+WidthSpace*2+mainForm.S1_GenJavaCodeBtn.Width(), top, WindowBtnWidth, WindowBtnHeight)
		mainForm.S1_GenGoCodeBtn.SetCaption("生成Go代码")
		mainForm.S1_GenGoCodeBtn.SetOnClick(func(_ vcl.IObject) {
			fmt.Println("暂未实现,生成Go代码")
		})

		mainForm.S1_OpenBasePathBtn = vcl.NewButton(mainForm)
		mainForm.S1_OpenBasePathBtn.SetParent(sheet)
		mainForm.S1_OpenBasePathBtn.SetBounds(mainForm.S1_GenGoCodeBtn.Left()+WidthSpace*2+mainForm.S1_GenGoCodeBtn.Width(), top, WindowBtnWidth, WindowBtnHeight)
		mainForm.S1_OpenBasePathBtn.SetCaption("打开目录")
		mainForm.S1_OpenBasePathBtn.SetOnClick(func(_ vcl.IObject) {
			fmt.Println("暂未实现,打开目录")
		})
	}

	// 设置面板[SDK管理]
	if S2 {
		sheet = vcl.NewTabSheet(mainForm)
		sheet.SetPageControl(pgc)
		sheet.SetCaption("SDK管理")
		sheet.SetOnShow(func(sender vcl.IObject) {
			go mainForm.renderS2Sheet()
		})
		// 初始化头部信息
		top = 20
		S2_RootPathLabel := vcl.NewLabel(mainForm)
		S2_RootPathLabel.SetParent(sheet)
		S2_RootPathLabel.SetBounds(WidthSpace, top+LabelFix, LabelWidth, EmptyWidth)
		S2_RootPathLabel.SetCaption("SDK生效目录")

		mainForm.S2_RootPathEdit = vcl.NewEdit(mainForm)
		mainForm.S2_RootPathEdit.SetParent(sheet)
		mainForm.S2_RootPathEdit.SetReadOnly(true)
		mainForm.S2_RootPathEdit.SetBounds(S2_RootPathLabel.Left()+S2_RootPathLabel.Width()+WidthSpace, top, EditWidth, EmptyWidth)

		mainForm.S2_ChooseRootPathBtn = vcl.NewButton(mainForm)
		mainForm.S2_ChooseRootPathBtn.SetParent(sheet)
		mainForm.S2_ChooseRootPathBtn.SetBounds(mainForm.S2_RootPathEdit.Left()+mainForm.S2_RootPathEdit.Width()+FixWidthSpace, top, FormBtnWidth, FormBtnHeight)
		mainForm.S2_ChooseRootPathBtn.SetCaption("选择")
		mainForm.S2_ChooseRootPathBtn.SetOnClick(func(_ vcl.IObject) {
			chooseFolder(mainForm.S2_RootPathEdit)
		})

		top += CommonHeight
		S2_RepoPathLabel := vcl.NewLabel(mainForm)
		S2_RepoPathLabel.SetParent(sheet)
		S2_RepoPathLabel.SetBounds(WidthSpace, top+LabelFix, LabelWidth, EmptyWidth)
		S2_RepoPathLabel.SetCaption("SDK存储目录")

		mainForm.S2_RepoPathEdit = vcl.NewEdit(mainForm)
		mainForm.S2_RepoPathEdit.SetParent(sheet)
		mainForm.S2_RepoPathEdit.SetReadOnly(true)
		mainForm.S2_RepoPathEdit.SetBounds(S2_RepoPathLabel.Left()+S2_RepoPathLabel.Width()+WidthSpace, top, EditWidth, EmptyWidth)

		mainForm.S2_ChooseRepoPathBtn = vcl.NewButton(mainForm)
		mainForm.S2_ChooseRepoPathBtn.SetParent(sheet)
		mainForm.S2_ChooseRepoPathBtn.SetBounds(mainForm.S2_RepoPathEdit.Left()+mainForm.S2_RepoPathEdit.Width()+FixWidthSpace, top, FormBtnWidth, FormBtnHeight)
		mainForm.S2_ChooseRepoPathBtn.SetCaption("选择")
		mainForm.S2_ChooseRepoPathBtn.SetOnClick(func(_ vcl.IObject) {
			chooseFolder(mainForm.S2_RepoPathEdit)
		})

		top += CommonHeight
		S2_NodejsLabel := vcl.NewLabel(mainForm)
		S2_NodejsLabel.SetParent(sheet)
		S2_NodejsLabel.SetBounds(WidthSpace, top+LabelFix, LabelWidth, CommonHeight)
		S2_NodejsLabel.SetCaption("NodeJS管理")

		mainForm.S2_GetNodeJSLSVBtn = vcl.NewButton(mainForm)
		mainForm.S2_GetNodeJSLSVBtn.SetParent(sheet)
		mainForm.S2_GetNodeJSLSVBtn.SetBounds(S2_NodejsLabel.Left()+S2_NodejsLabel.Width()+WidthSpace, top, WindowBtnWidth, WindowBtnHeight)
		mainForm.S2_GetNodeJSLSVBtn.SetCaption("获取最新版本")
		mainForm.S2_GetNodeJSLSVBtn.SetOnClick(func(_ vcl.IObject) {
			go GetNodeJSLSV()
		})

		mainForm.S2_GetNodeJSVersionBtn = vcl.NewButton(mainForm)
		mainForm.S2_GetNodeJSVersionBtn.SetParent(sheet)
		mainForm.S2_GetNodeJSVersionBtn.SetBounds(mainForm.S2_GetNodeJSLSVBtn.Left()+mainForm.S2_GetNodeJSLSVBtn.Width()+WidthSpace, top, WindowBtnWidth, WindowBtnHeight)
		mainForm.S2_GetNodeJSVersionBtn.SetCaption("获取当前版本")
		mainForm.S2_GetNodeJSVersionBtn.SetOnClick(func(_ vcl.IObject) {
			go GetNodeJSVersion()
		})

		mainForm.S2_DownloadNodeJSBtn = vcl.NewButton(mainForm)
		mainForm.S2_DownloadNodeJSBtn.SetParent(sheet)
		mainForm.S2_DownloadNodeJSBtn.SetBounds(mainForm.S2_GetNodeJSVersionBtn.Left()+mainForm.S2_GetNodeJSVersionBtn.Width()+WidthSpace, top, WindowBtnWidth, WindowBtnHeight)
		mainForm.S2_DownloadNodeJSBtn.SetCaption("安装NodeJS")
		mainForm.S2_DownloadNodeJSBtn.SetOnClick(func(_ vcl.IObject) {
			repoPath := mainForm.S2_RepoPathEdit.Text()
			// 打开新窗口进行下载
			downloadForm := vcl.NewForm(mainForm)
			downloadForm.SetCaption("下载窗口")
			downloadForm.SetPosition(types.PoScreenCenter)
			downloadForm.SetWidth(400)
			downloadForm.SetHeight(120)

			versionEdit := vcl.NewEdit(downloadForm)
			versionEdit.SetParent(downloadForm)
			versionEdit.SetBounds(30, 20, 220, 0)

			downloadProcess := vcl.NewProgressBar(downloadForm)
			downloadProcess.SetParent(downloadForm)
			downloadProcess.SetBounds(30, 75, 340, 20)
			downloadProcess.SetMax(100)

			downloadNodeJSBtn := vcl.NewButton(downloadForm)
			downloadNodeJSBtn.SetParent(downloadForm)
			downloadNodeJSBtn.SetBounds(270, 20, 100, 23)
			downloadNodeJSBtn.SetCaption("安装")
			downloadNodeJSBtn.SetOnClick(func(_ vcl.IObject) {
				fileName, downloadUrl := GetDownloadUrl(versionEdit.Text())
				if len(fileName) == 0 {
					return
				}
				filePath := filepath.Join(repoPath, "nodejs")
				go DownloadFile(downloadUrl, filePath, fileName, downloadProcess)
			})

			downloadForm.Show()
			downloadForm.SetOnClose(func(sender vcl.IObject, action *types.TCloseAction) {
				// 检查是否有正在进行的下载任务
				fmt.Println("关闭窗口")
			})
		})
	}

	// 设置面板[磁力链接搜索]
	if S3 {
		sheet = vcl.NewTabSheet(mainForm)
		sheet.SetPageControl(pgc)
		sheet.SetCaption("磁力链接搜索")
		sheet.SetOnShow(func(sender vcl.IObject) {
			mainForm.S3_MagnetHostComboBox.SetItemIndex(int32(0))
		})
		// 初始化头部信息
		top = 20
		mainForm.S3_MagnetHostComboBox = vcl.NewComboBox(mainForm)
		mainForm.S3_MagnetHostComboBox.SetParent(sheet)
		mainForm.S3_MagnetHostComboBox.SetBounds(WidthSpace, top, ComboBoxWidth, EmptyWidth)
		mainForm.S3_MagnetHostComboBox.SetStyle(types.CsDropDownList)
		mainForm.S3_MagnetHostComboBox.Items().Add(magnet.CiliwangClub)
		mainForm.S3_MagnetHostComboBox.Items().Add(magnet.CiliguoCc)

		mainForm.S3_MagnetSearchKeyEdit = vcl.NewEdit(mainForm)
		mainForm.S3_MagnetSearchKeyEdit.SetParent(sheet)
		mainForm.S3_MagnetSearchKeyEdit.SetText("火影忍者")
		MagnetSearchKeyEditWidth := Width - mainForm.S3_MagnetHostComboBox.Left() - mainForm.S3_MagnetHostComboBox.Width() - WidthSpace - WidthSpace - WindowBtnWidth*2 - WidthSpace*2
		mainForm.S3_MagnetSearchKeyEdit.SetBounds(mainForm.S3_MagnetHostComboBox.Left()+mainForm.S3_MagnetHostComboBox.Width()+WidthSpace, top, MagnetSearchKeyEditWidth, EmptyWidth)
		mainForm.S3_MagnetSearchBtn = vcl.NewButton(mainForm)
		mainForm.S3_MagnetSearchBtn.SetParent(sheet)
		mainForm.S3_MagnetSearchBtn.SetBounds(mainForm.S3_MagnetSearchKeyEdit.Left()+mainForm.S3_MagnetSearchKeyEdit.Width()+WidthSpace, top, WindowBtnWidth, WindowBtnHeight)
		mainForm.S3_MagnetSearchBtn.SetCaption("搜索")
		mainForm.S3_MagnetSearchBtn.SetOnClick(func(_ vcl.IObject) {
			// 异步执行搜索
			mainForm.S3_MagnetListTable.SetCells(0, 1, "测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试")
			mainForm.S3_MagnetListTable.SetCells(1, 1, "测试测试测试测试测试测试测试")
			mainForm.S3_MagnetListTable.SetCells(2, 1, "测试测试测试测试测试测试测试测试测试")
			mainForm.S3_MagnetListTable.SetCells(3, 1, "测试测试测试测试测试测试测试测试测试测试测试")
		})

		mainForm.S3_MagnetSearchNextBtn = vcl.NewButton(mainForm)
		mainForm.S3_MagnetSearchNextBtn.SetParent(sheet)
		mainForm.S3_MagnetSearchNextBtn.SetBounds(mainForm.S3_MagnetSearchBtn.Left()+mainForm.S3_MagnetSearchBtn.Width()+WidthSpace, top, WindowBtnWidth, WindowBtnHeight)
		mainForm.S3_MagnetSearchNextBtn.SetCaption("下一页")
		mainForm.S3_MagnetSearchNextBtn.SetOnClick(func(_ vcl.IObject) {
		})

		top += CommonHeight
		mainForm.RenderMagnetTable(sheet, top)
	}

	// 设置面板[开发小工具]
	if S4 {
		sheet = vcl.NewTabSheet(mainForm)
		sheet.SetPageControl(pgc)
		sheet.SetCaption("开发小工具")
		sheet.SetOnShow(func(sender vcl.IObject) {
		})
		// 初始化头部信息
		top = 20
		S4_Ip2RegionLabel := vcl.NewLabel(mainForm)
		S4_Ip2RegionLabel.SetParent(sheet)
		S4_Ip2RegionLabel.SetBounds(WidthSpace, top+LabelFix, LabelWidth, EmptyWidth)
		S4_Ip2RegionLabel.SetCaption("IP地址")

		mainForm.S4_Ip2RegionEdit = vcl.NewEdit(mainForm)
		mainForm.S4_Ip2RegionEdit.SetParent(sheet)
		mainForm.S4_Ip2RegionEdit.SetText("127.0.0.1")
		mainForm.S4_Ip2RegionEdit.SetBounds(S4_Ip2RegionLabel.Left()+S4_Ip2RegionLabel.Width()+WidthSpace, top, EditWidth, EmptyWidth)

		mainForm.S4_Ip2RegionBtn = vcl.NewButton(mainForm)
		mainForm.S4_Ip2RegionBtn.SetParent(sheet)
		mainForm.S4_Ip2RegionBtn.SetBounds(mainForm.S4_Ip2RegionEdit.Left()+mainForm.S4_Ip2RegionEdit.Width()+FixWidthSpace, top, FormBtnWidth, FormBtnHeight)
		mainForm.S4_Ip2RegionBtn.SetCaption("查询地址")
		mainForm.S4_Ip2RegionBtn.SetOnClick(func(_ vcl.IObject) {
			if region, err := NewIp2Region(Ip2RegionPath); err != nil {
				vcl.ShowMessage(err.Error())
				return
			} else {
				defer region.Close()
				ipAddress := mainForm.S4_Ip2RegionEdit.Text()
				if len(ipAddress) > 0 {
					region.BtreeSearchForUI(ipAddress)
				}
			}
		})

		top += CommonHeight

		// 查询指定目录,指定字符串存在文件
		// 先选择文件夹,然后弹窗.像IDEA一样进行搜索
	}

	// 设置面板[加密解密]
	if S5 {
		sheet = vcl.NewTabSheet(mainForm)
		sheet.SetPageControl(pgc)
		sheet.SetCaption("加密解密")
		sheet.SetOnShow(func(sender vcl.IObject) {
			mainForm.S5_EncodeSourceMemo.SetText("")
			mainForm.S5_EncodeTypeComboBox.SetItemIndex(int32(0))
			mainForm.S5_EncodeResultMemo.SetText("")
			// 允许拖拽文件
			mainForm.SetAllowDropFiles(true)
		})
		sheet.SetOnHide(func(sender vcl.IObject) {
			mainForm.SetAllowDropFiles(false)
		})
		// 初始化头部信息
		top = 20
		mainForm.S5_EncodeSourceMemo = vcl.NewMemo(mainForm)
		mainForm.S5_EncodeSourceMemo.SetParent(sheet)
		mainForm.S5_EncodeSourceMemo.SetBounds(WidthSpace, top, Width-WidthSpace-WidthSpace, (Height-123)/2)

		top += mainForm.S5_EncodeSourceMemo.Height() + 20
		mainForm.S5_EncodeTypeComboBox = vcl.NewComboBox(mainForm)
		mainForm.S5_EncodeTypeComboBox.SetParent(sheet)
		mainForm.S5_EncodeTypeComboBox.SetBounds(WidthSpace, top, ComboBoxWidth, EmptyWidth)
		mainForm.S5_EncodeTypeComboBox.SetStyle(types.CsDropDownList)
		mainForm.S5_EncodeTypeComboBox.Items().Add(encode.MD5)
		mainForm.S5_EncodeTypeComboBox.Items().Add(encode.Base64)
		mainForm.S5_EncodeTypeComboBox.Items().Add(encode.Url)

		mainForm.S5_EncodeBtn = vcl.NewButton(mainForm)
		mainForm.S5_EncodeBtn.SetParent(sheet)
		mainForm.S5_EncodeBtn.SetBounds(mainForm.S5_EncodeTypeComboBox.Left()+mainForm.S5_EncodeTypeComboBox.Width()+FixWidthSpace, top, FormBtnWidth, FormBtnHeight)
		mainForm.S5_EncodeBtn.SetCaption("加密")
		mainForm.S5_EncodeBtn.SetOnClick(func(_ vcl.IObject) {
			mainForm.S5_EncodeBtn.SetCaption("加密中")
			mainForm.S5_EncodeBtn.SetEnabled(false)
			mainForm.S5_EncodeResultMemo.Clear()
			go func() {
				if encodeResult, err := encode.Handlers[mainForm.S5_EncodeTypeComboBox.Text()].EncodeData(DeleteSpecialChar(mainForm.S5_EncodeSourceMemo.Text())); err != nil {
					vcl.ShowMessage("加密失败")
					vcl.ThreadSync(func() {
						mainForm.S5_EncodeResultMemo.SetText("")
					})
				} else {
					vcl.ThreadSync(func() {
						mainForm.S5_EncodeResultMemo.SetText(encodeResult)
					})
				}
				vcl.ThreadSync(func() {
					mainForm.S5_EncodeBtn.SetCaption("加密")
					mainForm.S5_EncodeBtn.SetEnabled(true)
				})
			}()
		})

		mainForm.S5_DecodeBtn = vcl.NewButton(mainForm)
		mainForm.S5_DecodeBtn.SetParent(sheet)
		mainForm.S5_DecodeBtn.SetBounds(mainForm.S5_EncodeBtn.Left()+mainForm.S5_EncodeBtn.Width()+FixWidthSpace, top, FormBtnWidth, FormBtnHeight)
		mainForm.S5_DecodeBtn.SetCaption("解密")
		mainForm.S5_DecodeBtn.SetOnClick(func(_ vcl.IObject) {
			mainForm.S5_DecodeBtn.SetCaption("解密中")
			mainForm.S5_DecodeBtn.SetEnabled(false)
			go func() {
				if decodeResult, err := encode.Handlers[mainForm.S5_EncodeTypeComboBox.Text()].DecodeData(mainForm.S5_EncodeResultMemo.Text()); err != nil {
					vcl.ShowMessage("解密失败或者该类型不支持解密")
					vcl.ThreadSync(func() {
						mainForm.S5_EncodeSourceMemo.SetText("")
					})
				} else {
					vcl.ThreadSync(func() {
						mainForm.S5_EncodeSourceMemo.SetText(decodeResult)
					})
				}
				vcl.ThreadSync(func() {
					mainForm.S5_DecodeBtn.SetCaption("解密")
					mainForm.S5_DecodeBtn.SetEnabled(true)
				})
			}()
		})

		top += 43
		mainForm.S5_EncodeResultMemo = vcl.NewMemo(mainForm)
		mainForm.S5_EncodeResultMemo.SetParent(sheet)
		mainForm.S5_EncodeResultMemo.SetBounds(WidthSpace, top, Width-WidthSpace-WidthSpace, (Height-123)/2)
	}

	if S6 {
		sheet = vcl.NewTabSheet(mainForm)
		sheet.SetPageControl(pgc)
		sheet.SetCaption("解析包")
		sheet.SetOnShow(func(sender vcl.IObject) {
		})
		sheet.SetOnHide(func(sender vcl.IObject) {
		})
		// 初始化头部信息
		top = 20
	}
}

func chooseFolder(edit *vcl.TEdit) {
	options := types.NewSet(types.SdNewFolder, types.SdShowEdit, types.SdNewUI)
	if ok, dir := vcl.SelectDirectory1(options); ok {
		edit.SetText(dir)
	}
}

// [S1]加载[代码生成器][Tab]页面数据
func (mainForm *MainForm) renderS1Sheet() {
	var (
		err                  error
		coderConfigModelJson []byte
		coderConfigModel     CoderConfigModel
	)
	mainForm.S1_DataUrlEdit.SetText(EmptyString)
	mainForm.S1_DataNameEdit.SetText(EmptyString)
	mainForm.S1_DataUsernameEdit.SetText(EmptyString)
	mainForm.S1_DataPasswordEdit.SetText(EmptyString)
	mainForm.S1_BasePathEdit.SetText(EmptyString)
	mainForm.S1_BasePackageNameEdit.SetText(EmptyString)
	mainForm.S1_PackageNameEdit.SetText(EmptyString)
	mainForm.S1_TablesEdit.SetText(EmptyString)
	if coderConfigModelJson, err = Data.Get([]byte(CodeConfigModelKey), nil); err != nil {
		return
	}
	if err := json.Unmarshal(coderConfigModelJson, &coderConfigModel); err != nil {
		return
	}
	mainForm.S1_DataUrlEdit.SetText(coderConfigModel.DataUrlEdit)
	mainForm.S1_DataNameEdit.SetText(coderConfigModel.DataNameEdit)
	mainForm.S1_DataUsernameEdit.SetText(coderConfigModel.DataUsernameEdit)
	mainForm.S1_DataPasswordEdit.SetText(coderConfigModel.DataPasswordEdit)
	mainForm.S1_BasePathEdit.SetText(coderConfigModel.BasePathEdit)
	mainForm.S1_BasePackageNameEdit.SetText(coderConfigModel.BasePackageNameEdit)
	mainForm.S1_PackageNameEdit.SetText(coderConfigModel.PackageNameEdit)
	mainForm.S1_TablesEdit.SetText(coderConfigModel.TablesEdit)
}

// [S1]渲染数据库表列表[异步]
func (mainForm *MainForm) asyncRenderTables(tablesChkListBox *vcl.TCheckListBox) {
	// 获取数据库表
	err, tables := GetTables(DataBaseConfig{
		Host:     mainForm.S1_DataUrlEdit.Text(),
		Username: mainForm.S1_DataUsernameEdit.Text(),
		Password: mainForm.S1_DataPasswordEdit.Text(),
		DbName:   mainForm.S1_DataNameEdit.Text(),
	})
	if err != nil {
		vcl.ShowMessage("获取数据表信息失败" + LineBreak + err.Error())
	}
	// 切换到主线程更新UI
	vcl.ThreadSync(func() {
		// 渲染数据库表列表
		for i := 0; i < len(tables); i++ {
			tablesChkListBox.Items().Add(tables[i])
		}
	})
}

// [S2]加载[SDK管理][Tab]页面数据
func (mainForm *MainForm) renderS2Sheet() {
	var (
		err                error
		semConfigModelJson []byte
		semConfigModel     SemConfigModel
	)
	mainForm.S2_RootPathEdit.SetText(EmptyString)
	mainForm.S2_RepoPathEdit.SetText(EmptyString)
	if semConfigModelJson, err = Data.Get([]byte(SemConfigModelKey), nil); err != nil {
		return
	}
	if err := json.Unmarshal(semConfigModelJson, &semConfigModel); err != nil {
		return
	}
	mainForm.S2_RootPathEdit.SetText(semConfigModel.RootPath)
	mainForm.S2_RepoPathEdit.SetText(semConfigModel.RepoPath)
}

//[S3] 构建磁力链接搜索结果表格
func (mainForm *MainForm) RenderMagnetTable(sheet *vcl.TTabSheet, top int32) {
	mainForm.S3_MagnetListTable = vcl.NewStringGrid(mainForm)
	mainForm.S3_MagnetListTable.SetParent(sheet)
	mainForm.S3_MagnetListTable.SetBounds(WidthSpace, top, Width-WidthSpace-WidthSpace, Height-36-top)
	mainForm.S3_MagnetListTable.SetFixedCols(0)
	mainForm.S3_MagnetListTable.SetFixedRows(1)
	mainForm.S3_MagnetListTable.SetRowCount(17)
	mainForm.S3_MagnetListTable.SetColCount(4)
	mainForm.S3_MagnetListTable.SetCells(0, 0, "名称")
	mainForm.S3_MagnetListTable.SetCells(1, 0, "大小")
	mainForm.S3_MagnetListTable.SetCells(2, 0, "热度")
	mainForm.S3_MagnetListTable.SetCells(3, 0, "发布时间")
	mainForm.S3_MagnetListTable.SetColWidths(0, 300)
	mainForm.S3_MagnetListTable.SetColWidths(1, 150)
	mainForm.S3_MagnetListTable.SetColWidths(2, 136)
	mainForm.S3_MagnetListTable.SetColWidths(3, 150)
}

// [S5]监听拖拽文件事件
func (mainForm *MainForm) OnFormDropFiles(_ vcl.IObject, aFileNames []string) {
	mainForm.S5_EncodeSourceMemo.Clear()
	mainForm.S5_EncodeSourceMemo.Lines().Add(aFileNames[0])
}
