package main

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

const (
	QueryTableList string = "SELECT DISTINCT TABLE_NAME FROM information_schema.`COLUMNS` WHERE TABLE_SCHEMA = ?"
)

type Table struct {
	TableName string `db:"TABLE_NAME"`
}

type DataBaseConfig struct {
	Host     string
	Username string
	Password string
	DbName   string
}

func TestDataBaseConn(config DataBaseConfig) error {
	ConnectUrl := config.Username + ":" + config.Password + "@tcp(" + config.Host + ":3306)/" + config.DbName + "?charset=utf8&parseTime=True&loc=Local"
	if data, err := sqlx.Open("mysql", ConnectUrl); err != nil {
		fmt.Println("数据库初始化失败", err)
		return err
	} else {
		return data.Ping()
	}
}

// 获取待生成代码的表信息
func GetTables(config DataBaseConfig) (error, []string) {
	var tableList []string
	ConnectUrl := config.Username + ":" + config.Password + "@tcp(" + config.Host + ":3306)/" + config.DbName + "?charset=utf8&parseTime=True&loc=Local"
	DataBase, _ := sqlx.Open("mysql", ConnectUrl)
	var tableMapList []Table
	if err := DataBase.Select(&tableMapList, QueryTableList, config.DbName); err != nil {
		return err, tableList
	}
	for _, tableMap := range tableMapList {
		tableList = append(tableList, tableMap.TableName)
	}
	return nil, tableList
}
