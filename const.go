package main

import (
	rotateLogs "github.com/lestrrat-go/file-rotatelogs"
	"github.com/pkg/errors"
	"github.com/rifflock/lfshook"
	log "github.com/sirupsen/logrus"
	"os"
	"path"
	"time"
)

const Ip2RegionPath = "plugins/ip2region.db"
const LogPath = "log"
const LogName = "toolkit"

const IsInit = "IsInit"

const LineBreak = "\r\n"

const (
	S1 bool = true
	S2 bool = true
	S3 bool = true
	S4 bool = true
	S5 bool = true
	S6 bool = true
)

const (
	EmptyString = ""
)

const (
	CodeConfigModelKey = "CodeConfigModelKey"
	SemConfigModelKey  = "SemConfigModelKey"
)

func init() {
	// 配置日志参数
	log.SetLevel(log.InfoLevel)
	log.AddHook(newRotateHook(LogPath, LogName, 7*24*time.Hour, 24*time.Hour))
}

func newRotateHook(logPath string, logFileName string, maxAge time.Duration, rotationTime time.Duration) *lfshook.LfsHook {
	if _, err := os.Stat(logPath); err != nil {
		_ = os.MkdirAll(logPath, 0775)
	}
	baseLogPath := path.Join(logPath, logFileName)

	writer, err := rotateLogs.New(
		baseLogPath+".%Y-%m-%d.log",
		rotateLogs.WithLinkName(baseLogPath),
		rotateLogs.WithMaxAge(maxAge),
		rotateLogs.WithRotationTime(rotationTime),
	)
	if err != nil {
		log.Errorf("config local file system logger error. %+v", errors.WithStack(err))
	}
	return lfshook.NewHook(lfshook.WriterMap{
		log.DebugLevel: writer,
		log.InfoLevel:  writer,
		log.WarnLevel:  writer,
		log.ErrorLevel: writer,
		log.FatalLevel: writer,
		log.PanicLevel: writer,
	}, &log.TextFormatter{DisableColors: true, TimestampFormat: "2006-01-02 15:04:05.000"})
}
