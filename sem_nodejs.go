package main

import (
	"encoding/json"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/ying32/govcl/vcl"
	"net/http"
	"os/exec"
	"runtime"
)

// 获取NodeJS稳定版本
func GetNodeJSLSV() {
	var (
		err      error
		response *http.Response
		document *goquery.Document
	)
	if response, err = http.Get("http://nodejs.cn/download/"); err != nil {
		vcl.ShowMessage("获取最新版本失败\r\n" + err.Error())
		return
	}
	defer response.Body.Close()
	if response.StatusCode != 200 {
		vcl.ShowMessage("获取最新版本失败\r\nerror: 获取最新稳定版本失败,请检查网络连接")
		return
	}
	if document, err = goquery.NewDocumentFromReader(response.Body); err != nil {
		vcl.ShowMessage("获取最新版本失败\r\n" + err.Error())
		return
	}
	document.Find("#main > div > article > section > p").Each(func(i int, ele *goquery.Selection) {
		vcl.ShowMessage(ele.Text())
	})
	return
}

// 获取当前NodeJS版本
func GetNodeJSVersion() {
	cmd := exec.Command("node", "-v")
	if version, err := cmd.CombinedOutput(); err != nil {
		vcl.ShowMessage("获取当前版本失败" + LineBreak + err.Error())
	} else {
		vcl.ShowMessage(DeleteSpecialChar(string(version)))
	}
}

func GetDownloadUrl(version string) (fileName, downloadUrl string) {
	var (
		err                error
		semConfigModelJson []byte
		semConfigModel     SemConfigModel
	)
	if semConfigModelJson, err = Data.Get([]byte(SemConfigModelKey), nil); err != nil {
		return fileName, EmptyString
	}
	if err := json.Unmarshal(semConfigModelJson, &semConfigModel); err != nil {
		return fileName, EmptyString
	}
	version = "v" + version
	if runtime.GOOS == "windows" {
		fileName = fmt.Sprintf("node-%s-%s-x%d.%s", version, "win", GetArch(), "zip")
	} else {
		fileName = fmt.Sprintf("node-%s-%s-x%d.%s", version, "linux", GetArch(), "tar.gz")
	}
	return fileName, fmt.Sprintf(semConfigModel.NodeJSMirror+"node/%s/%s", version, fileName)
}
