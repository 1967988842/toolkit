package main

import (
	"encoding/json"
	"github.com/syndtr/goleveldb/leveldb"
)

const DataPath = "data"

var Data *leveldb.DB

type CoderConfigModel struct {
	DataUrlEdit         string
	DataNameEdit        string
	DataUsernameEdit    string
	DataPasswordEdit    string
	BasePathEdit        string
	BasePackageNameEdit string
	PackageNameEdit     string
	TablesEdit          string
}

type SemConfigModel struct {
	RootPath     string
	RepoPath     string
	NodeJSMirror string
}

func DataInit() {
	Data, _ = leveldb.OpenFile(DataPath, nil)
	_, err := Data.Get([]byte(IsInit), nil)
	if err != nil {
		InitCoderConfig()
		InitSemConfig()
	}
}

func setIsInit() {
	_ = Data.Put([]byte(IsInit), []byte(IsInit), nil)
}

func RemoveIsInit() {
	_ = Data.Delete([]byte(IsInit), nil)
}

// 初始化代码生成器相关的配置
func InitCoderConfig() {
	coderConfigModel, err := json.Marshal(CoderConfigModel{
		DataUrlEdit:         "127.0.0.1",
		DataNameEdit:        "micro_devops",
		DataUsernameEdit:    "root",
		DataPasswordEdit:    "root",
		BasePathEdit:        "/Temp",
		BasePackageNameEdit: "com.example.api",
		PackageNameEdit:     "models",
		TablesEdit:          "",
	})
	if err != nil {
		return
	}
	_ = Data.Put([]byte(CodeConfigModelKey), coderConfigModel, nil)
}

// 初始化SDK(SDK Env Manager)管理相关的配置
func InitSemConfig() {
	semConfigModel, err := json.Marshal(SemConfigModel{
		RootPath:     "D:\\Tmp\\sdk",
		RepoPath:     "D:\\Tmp\\sdk\\repo",
		NodeJSMirror: "https://npm.taobao.org/mirrors/",
	})
	if err != nil {
		return
	}
	_ = Data.Put([]byte(SemConfigModelKey), semConfigModel, nil)
}
