package encode

// 统一接口和初始化各个类型的处理类

var Handlers = make(map[string]Encode)

type Handler struct {
	RunEncodeData func(str string) (string, error)
	RunDecodeData func(str string) (string, error)
}

func (handler *Handler) EncodeData(str string) (string, error) {
	return handler.RunEncodeData(str)
}

func (handler *Handler) DecodeData(str string) (string, error) {
	return handler.RunDecodeData(str)
}

type Encode interface {
	EncodeData(str string) (string, error)
	DecodeData(str string) (string, error)
}

const (
	MD5    = "MD5"
	Base64 = "Base64"
	Url    = "URL"
	Html   = "HTML"
)

func init() {
	Handlers[MD5] = MD5Handle
	Handlers[Base64] = Base64Handle
	Handlers[Url] = UrlHandle
}
