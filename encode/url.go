package encode

import (
	"net/url"
)

var UrlHandle = &Handler{RunEncodeData: func(str string) (string, error) {
	return url.QueryEscape(str), nil
}, RunDecodeData: func(str string) (string, error) {
	return url.QueryUnescape(str)
}}
