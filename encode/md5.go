package encode

import (
	"crypto/md5"
	"encoding/hex"
	"errors"
	"io"
	"os"
)

var MD5Handle = &Handler{RunEncodeData: func(str string) (string, error) {
	if md5InputFile, err := os.Open(str); err != nil {
		md5h := md5.New()
		md5h.Write([]byte(str))
		return hex.EncodeToString(md5h.Sum(nil)), nil
	} else {
		defer md5InputFile.Close()
		md5h := md5.New()
		_, _ = io.Copy(md5h, md5InputFile)
		return hex.EncodeToString(md5h.Sum(nil)), nil
	}
}, RunDecodeData: func(str string) (string, error) {
	return "", errors.New("该类型不支持解密操作")
}}
