package encode

import "encoding/base64"

var Base64Handle = &Handler{RunEncodeData: func(str string) (string, error) {
	return base64.StdEncoding.EncodeToString([]byte(str)), nil
}, RunDecodeData: func(str string) (string, error) {
	if decodeBytes, err := base64.StdEncoding.DecodeString(str); err != nil {
		return "", err
	} else {
		return string(decodeBytes), nil
	}
}}
